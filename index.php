<?php
require('animal.php');
require('Ape.php');
require('Frog.php');

$animal = new Animal ("shaun");
echo "Name : " . $animal->name . "<br>";
echo "Legs : " . $animal->legs . "<br>";
echo "Cold Blooded : " . $animal->cold_blooded . "<br>";
echo "<br>";
$Ape = new Ape("kera sakti");
echo "Name : " . $Ape->name . "<br>";
echo "Legs : " . $Ape->legs . "<br>";
echo "Yell : " . $Ape->yell() . "<br>";
echo "<br>";
$Frog = new Frog("buduk");
echo "Name : " . $Frog->name . "<br>";
echo "Legs : " . $Frog->legs . "<br>";
echo "Jump : " . $Frog->jump() . "<br>";
?>